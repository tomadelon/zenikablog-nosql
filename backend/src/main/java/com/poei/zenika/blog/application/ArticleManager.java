package com.poei.zenika.blog.application;

import com.poei.zenika.blog.model.Article;
import com.poei.zenika.blog.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleManager {
    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleManager(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Article createArticle(String title, String summary, String
            content){
        Article article = new Article(UUID.randomUUID().toString(),
                title, summary, content);
        articleRepository.save(article);
        return article;
    }

    public void deleteArticle(String id) {
        articleRepository.deleteById(id);
    }

    public Iterable<Article> getAllArticles(){
        return articleRepository.findAll();
    }

    public Optional<Article> getArticle(String articleId){
        return articleRepository.findById(articleId);
    }
}
