package com.poei.zenika.blog.web;

import com.poei.zenika.blog.application.ArticleManager;
import com.poei.zenika.blog.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/articles")
public class ArticleController {
    private final ArticleManager articleManager;

    @Autowired
    public ArticleController(ArticleManager articleManager) {
        this.articleManager = articleManager;
    }

    @PostMapping
    public Article createArticle(@RequestBody ArticleDto articleDto) {
        return
                this.articleManager.createArticle(articleDto.getTitle(),
                        articleDto.getSummary(), articleDto.getContent());
    }

    @GetMapping
    Iterable<Article> fetchArticles() {
        return this.articleManager.getAllArticles();
    }

    @GetMapping("/{articleId}")
    public Optional<Article>
    findArticleById(@PathVariable("articleId") String articleId) {
        return this.articleManager.getArticle(articleId);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteArticle(@PathVariable String id) {
        articleManager.deleteArticle(id);
    }


}
