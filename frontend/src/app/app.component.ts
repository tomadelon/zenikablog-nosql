import { Component,OnInit } from '@angular/core';
import axios from 'axios';
import { Article } from './model/article.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  public articles: Article[] = [];
  public articleId?: string;



  public fetchArticles() {
    axios.get("http://localhost:8080/api/articles")
    .then((res) => {
    console.log(res);
    this.articles =res.data;
     //res.data.forEach((api: Article) => {
     //console.log(res)  
    // this.articles.push(api);
    // });
    })
    .catch((err) => {
    console.log(err);
    });
  }

  public createArticle() {
    const apiUrl = 'http://localhost:8080/api/articles/';
    axios.post(`${apiUrl}`, { title: "Alexandre dans la prairie", summary: "Il aime gambader pieds nuds dans l'herbe", content: "essai essai",})
    .then((res) => {
     // window.location.reload();
      this.articles = [];
    console.log(res);
    })
    .catch((err) => {
    console.log(err);
    });
  }



  public deleteArticle(id: string) {
    console.log(id)
    const apiUrl = 'http://localhost:8080/api/articles/';
    axios.delete(`${apiUrl}${id}`)
    .then(() => {
     // window.location.reload();
    })
    .catch((err) => {
    console.log(err);
    });
    }
  

public ngOnInit(): void  {
    this.fetchArticles()
}

}
